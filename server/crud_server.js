const urlapi = require('url');
const fs = require('fs');
const http = require('http');
const mysql = require('mysql2');
const port = 3000;
const server = new http.Server();


const mysqlConnection = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "db_crud",
    password: ""
});


let personsMock = [];
let jsonMock = [];
const nameJsonFile = '../crud_db.json';
let typeStorage = 'Mock';

server.on('request', requestHandler);

async function requestHandler(request, response){
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.setHeader("Content-Type", "application/json, text/plain; charset=utf-8;");
    response.setHeader('Access-Control-Max-Age', '-1');
    response.statusCode = 200;

    readJsonFile();

    let url = urlapi.parse(request.url, true).pathname;
    typeStorage =  url.slice(1, url.length);
    // console.log('url: ' + url);
    // console.log('typeStorage: ' + typeStorage);
    let workId = urlapi.parse(request.url, true).search;
    if (workId){
        workId = workId.slice(1);
    }
    // console.log('workId: ' + workId);
    // console.log('request.method: ' + request.method);

    switch (request.method) {
        case "GET":
            switch (typeStorage) {

                case "Mock":
                    let mockData = JSON.stringify( personsMock.map((item, index) =>{
                        let obj = item;
                        obj['id'] = index + 1;
                        return obj;
                    }));
                    // console.log('mockData: ' + mockData);
                    response.write( mockData );
                    response.end();
                    break;

                case "JSON":
                    let jsonDate;
                    readJsonFile();
                    jsonDate = JSON.stringify( await jsonMock.map((item, index) =>{
                        let obj = item;
                        obj['id'] = index + 1;
                        return obj;
                    }));
                    response.write( jsonDate );
                    response.end();
                    break;

                case "mySQL":
                    let sql =`SELECT * FROM person`;
                    let mysqlDate;
                    mysqlConnection.query(sql,  async function (err, result) {
                        if (err){
                            console.error(err);
                        } else {
                            mysqlDate = await JSON.stringify(result);
                            console.log('mySQL return:' + mysqlDate);
                            response.write(mysqlDate);
                            response.end();
                        }
                    });
                    break;
            }
            break;

        case "POST":
            let post = '';
            request.on('data', function(chunk) {
                post += chunk.toString();
            });

            request.on('end', async function() {
                console.log('data: ' + post);
                let postData = JSON.parse(post);
                switch (typeStorage) {

                    case "Mock":
                        await personsMock.push(postData);
                        response.statusCode = 200;
                        response.end();
                        // console.log('personsMockPost: ' + personsMock);
                        break;

                    case "JSON":
                        await readJsonFile();
                        await jsonMock.push(postData);
                        saveJsonFile();
                        response.statusCode = 200;
                        response.end();
                        break;

                    case "mySQL":
                        let sql =`INSERT INTO person(first, last, age) VALUES("${postData.first}","${postData.last}","${postData.age}")`;
                        console.log('mySQL sql: '+ sql);
                        let mysqlDate;
                        mysqlConnection.query(sql,  async function (err, result) {
                            if (err){
                                console.error(err);
                            } else {
                                mysqlDate = await JSON.stringify(result);
                                console.log('mySQL return:' + mysqlDate);
                                response.write(mysqlDate);
                                response.end();
                            }
                        });
                        break;
                }
            });
            break;

        case "PUT":
            let put = '';

            request.on('data', function(chunk) {
                put += chunk.toString();
            });

            request.on('end', async function() {
                console.log('PUT data: ' + put);
                let putData = JSON.parse(put);
                switch (typeStorage) {

                    case "Mock":
                        personsMock.splice(workId - 1, 1, JSON.parse(put));
                        response.statusCode = 200;
                        response.end();
                        break;

                    case "JSON":
                        await readJsonFile();
                        jsonMock.splice(workId - 1, 1, JSON.parse(put));
                        saveJsonFile();
                        response.statusCode = 200;
                        response.end();
                        break;

                    case "mySQL":

                        let sql =`UPDATE person SET first="${putData.first}", last="${putData.last}", age="${putData.age}" WHERE id=${workId}`;
                        console.log('mySQL PUT sql: '+ sql);
                        mysqlConnection.query(sql, async function (err, result) {
                            if (err){
                                console.error(err);
                            } else {
                                response.write(await JSON.stringify(result));
                                response.end();
                            }
                        });
                        break;
                }
            });
            break;

        case "DELETE":
            console.log("DELETE id=" + workId);
            switch (typeStorage) {

                case "Mock":
                    personsMock.splice(workId - 1, 1);
                    response.statusCode = 200;
                    response.end();
                    break;

                case "JSON":
                    await readJsonFile();
                    jsonMock.splice(workId - 1, 1);
                    saveJsonFile();
                    response.statusCode = 200;
                    response.end();
                    break;
                case "mySQL":
                    // DELETE FROM users WHERE name=?
                    let sql =`DELETE FROM person WHERE id=${workId}`;
                    console.log('mySQL PUT sql: '+ sql);
                    mysqlConnection.query(sql, async function (err, result) {
                        if (err){
                            console.error(err);
                        } else {
                            response.write(await JSON.stringify(result));
                            response.end();
                        }
                    });
                    break;
            }
            break;

        case "OPTIONS":
            response.end();
            break;
        default:
            console.log('undefined method: '  + request.method);
            break;
    }
}

server.listen(port, (err) =>{
    if(err){
        return console.log(`error: ${err}`);
    } else {
        return console.log(`server port: ${port}`);
    }
});

//*************** json-file ***********************
function readJsonFile() {
    fs.readFile(nameJsonFile,'utf8',(err, data) => {
        if(!err){
            if (data){
                jsonMock = JSON.parse(data);
                // console.log('readJsonFile: ' +  data);
            }
        }else{
            console.error(err);
        }
    })
}

function saveJsonFile() {
    fs.writeFile(nameJsonFile, JSON.stringify(jsonMock), (err) => {
        if(err) throw err;
    })
}
//*************** json-file ***********************



// class MySingleton {
//     static instance = null;
//
//     static getInstance () {
//         instance = instance & instance : new DBConnector()
//         return instance;
//     }
// }
//
// //для получение коннекта с базой данных в любом месте, будет одна и та же сущность
// const connector = MySingleton.getInstance
//
// /******************использовать обзёрвер в групповом проекте***********************************************/
// //оповещает всех кто подписан на событие данными в обзёрвере
// class Observer {
//     this.subscribers = {}
//     //должны передать ЧТО передадим, и КУДА будем это передавать
//     //т.е. само событие кот. произошло и сами данные
//     publish(event, callback){
//         //if...
//
//         for(const emit of this.subscribers[event]){
//             emit(data);
//         }
//
//     }
//
//     //мы подписываемся на событие
//     subscribe(event, callback) {
//         //если не было обработок по этому ивенту
//         if(!this.subscribers[event]){
//             this.subsribers[event] = [];
//         }
//         this.subscribers[event].push(callback);
//
//     }
//     //проходимся по callback, если нашли удаляем сплайсом
//     unsubscribe(event, callback) {
//         fpr (let index = 0; index<this.subsribers[event].length; index++){
//             if(callback === this.subsribers[event][index]){
//                 //splice
//                 break
//             }
//         }
//
//     }
// }
