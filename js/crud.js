// POST добавляет новую запись в базу данных.
// GET получает запись из базы данных.
// PUT берет запись из базы и заменяет ее новой.
// DELETE удаляет запись из базы.

var typeStorage = 'Mock';


const tableList = document.getElementById('output');
const tableBody = document.querySelector('tbody');

const id = document.getElementById('id');
const first = document.getElementById('FirstName');
const last = document.getElementById('LastName');
const age = document.getElementById('Age');


let selectDB = document.getElementById('select');
let selectTxt = document.getElementById('select-db');
let btnCreate = document.getElementById('create');
let btnDelete = document.getElementById('delete');
let btnUpdate = document.getElementById('update');

selectDB.addEventListener('change', changeDB);
btnCreate.addEventListener('click', createRecord);
btnDelete.addEventListener('click', deleteRecord);
btnUpdate.addEventListener('click', updateRecord);

tableList.addEventListener('click', tableClick);

document.onloadend = getTables();

async function changeDB() {
    typeStorage = selectDB.value;
    await getTables();

}

function createBody() {
    if (validation()){
        return JSON.stringify({
            first: first.value,
            last: last.value,
            age: age.value,
        })
    }
}

async function createRecord(){
    let response = await requestServer(`http://localhost:3000/${typeStorage}`, 'POST', createBody());

    if (response.ok){
        console.log('OK, status: ' + response.status);
        await getTables();
    } else {
        console.log('ERROR' + response.status);
    }
}

async function deleteRecord() {
    let response = await requestServer(`http://localhost:3000/${typeStorage}?${id.value}`, 'DELETE');

    if (response.ok){
        console.log('OK, status: ' + response.status);
        let dataTable = await getTables();
        console.log(': ' + dataTable);
    } else {
        console.log('ERROR' + response.status);
    }
}

async function updateRecord(){
    let response = await requestServer(`http://localhost:3000/${typeStorage}?${id.value}`, 'PUT', createBody());

    if (response.ok){
        console.log('OK, status: ' + response.status);
        await getTables();
    } else {
        console.log('ERROR' + response.status);
    }
}

async function getTables() {
    let response = await requestServer(`http://localhost:3000/${typeStorage}`);
    if (response.ok){
        refreshTable(await response.json());
    } else {
        console.log('якась фиігня трапилась :(');
    }
    clearInputs();
}

function requestServer(url, method = 'GET', body = null) {
    return fetch(url, {
        method: method,
        body: body,
    })
}

function refreshTable(data) {
    tableBody.innerHTML = null;
    for (let row of data){
        let rowElement = () =>{
            let tr = document.createElement("tr");
            tr.className = 'outputTable__field';
            tr.id = `${row.id}`;
            tr.innerHTML = `
                    <th>${row.id}</th>
                    <th>${row.first}</th>
                    <th>${row.last}</th>
                    <th>${row.age}</th>`;
            return tr;
        };
        tableBody.append(rowElement());
    }
}

function tableClick(event) {

    const row = searchId(event.target.parentElement.id);

    id.value = row.children[0].textContent;
    first.value = row.children[1].textContent;
    last.value = row.children[2].textContent;
    age.value = row.children[3].textContent;
}

function searchId(id) { // находим и возвращаем строку с id, или null
    for (let tr of tableBody.children){
        if (tr.id === id){
            return tr;
        }
    }
    return null;
}

function clearInputs() {
    id.value = '';
    first.value = '';
    last.value = '';
    age.value = '';
}
//****************** OLD **********************************

function validation() { // проверяем корректнось ввода данных

    if (!Number.isInteger(Number(age.value))){
        alert('Age может быть только целым числом!');
        return false;
    }

    if(!first.value||!last.value||!age.value){
        alert('Заполните все поля');
        return false;
    }

    return true;
}

